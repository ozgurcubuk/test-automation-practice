@order
Feature: Order

  Scenario Outline: Sign in user and make an order
    Given I launched <xBrowser> browser and navigate to <yPage> page
    When I sign in a new user
    And Add a product from Summer Dresses category
    And Search product with 'summer' key and add to chart
    And Check out chart
    And I proceed with entering address
    And I proceed with shipping options
    And I proceed with payment options
    Then I check order history and details
    Examples:
      | xBrowser | yPage                          |
      | chrome   | http://automationpractice.com/ |

