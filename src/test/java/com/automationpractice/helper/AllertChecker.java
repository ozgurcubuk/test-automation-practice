package com.automationpractice.helper;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AllertChecker {

    WebDriver driver;

    @FindBy(how = How.ID, using = "create_account_error")
    WebElement email_error;

    public AllertChecker(WebDriver driver) {
        this.driver = driver;
    }

    public boolean isAllertRaised() {
        try {
            email_error.isDisplayed();
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
