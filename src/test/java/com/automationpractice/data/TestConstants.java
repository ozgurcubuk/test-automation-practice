package com.automationpractice.data;

public interface TestConstants {

    String MR = "MR";

    String MRS = "MRS";

    String FIRST_NAME = "ÖZGÜR";

    String LAST_NAME = "ÇUBUK";

    String PASSWORD = "12345";

    String COMPANY = "MyCompany";

    String ADDRESS1 = "Berkeley Street";

    String ADDRESS2 = "N:8";

    String CITY = "San Francisco";

    String POSTAL_CODE = "34000";

    String HOME_PHONE = "+902120010101";

    String MOBILE_PHONE = "+905310010101";

    String ADDRESS_ALIAS = "myAddressAllias";

    String STATE = "California";
}
