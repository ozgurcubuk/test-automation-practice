package com.automationpractice.stepdefs;

import com.automationpractice.factory.AbstractBrowserFactory;
import com.automationpractice.page.CategoryPage;
import com.automationpractice.page.ChartPage;
import com.automationpractice.page.MyAccountPage;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.PageFactory;

public class Product extends AbstractBrowserFactory {

    MyAccountPage myAccountPage = PageFactory.initElements(driver, MyAccountPage.class);
    CategoryPage categoryPage = PageFactory.initElements(driver, CategoryPage.class);
    ChartPage chartPage = PageFactory.initElements(driver, ChartPage.class);

    @When("^Add a product from Summer Dresses category$")
    public void whenIAddAProductFromSummerDressesCategory() {
        myAccountPage.clickToDressesCategory();
        categoryPage.addProductToChart();
    }

    @When("^Search product with '(.*)' key and add to chart$")
    public void whenISearchProductAndAddToChart(String searchQuery) {
        categoryPage.searchProductAndAddToChart(searchQuery);
    }

    @When("^Check out chart$")
    public void whenICheckOutChart() {
        categoryPage.checkOutChart();
    }

    @When("^I proceed with entering address$")
    public void whenIProceedWithEnteringAddress() {
        chartPage.continueToAddressSection();
    }

    @When("^I proceed with shipping options$")
    public void whenIProceedWithShippingOptions() {
        chartPage.continueToShippingSectionAndAggreeTerms();
    }

    @When("^I proceed with payment options$")
    public void whenIProceedWithPaymentOptions() {
        chartPage.continueToPaymentSection();
    }
}
