package com.automationpractice.stepdefs;

import com.automationpractice.factory.AbstractBrowserFactory;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;

public class Browser extends AbstractBrowserFactory {

    WebDriver driver;

    @Given("^I launched (.*) browser and navigate to (.*) page$")
    public void whenILaunchedXBrowserAndNavigateToY(String browser, String url) {
        driver = launchBrowser(browser, url);
    }
}
