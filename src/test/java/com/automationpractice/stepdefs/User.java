package com.automationpractice.stepdefs;

import com.automationpractice.factory.AbstractBrowserFactory;
import com.automationpractice.page.MainPage;
import com.automationpractice.page.MyAccountPage;
import com.automationpractice.page.SignInPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import static com.automationpractice.data.TestConstants.*;

public class User extends AbstractBrowserFactory {

    MainPage mainPage = PageFactory.initElements(driver, MainPage.class);
    SignInPage signInPage = PageFactory.initElements(driver, SignInPage.class);
    MyAccountPage myAccountPage = PageFactory.initElements(driver, MyAccountPage.class);

    @When("^I sign in a new user$")
    public void whenISignInANewUser() {
        mainPage.clickToSignInButton();
        signInPage.enterAnEmailAndClickToCreateAnAccountButton();
        signInPage.fillInUserInformations(MR, FIRST_NAME, LAST_NAME, PASSWORD, COMPANY, ADDRESS1, ADDRESS2, CITY,
                POSTAL_CODE, HOME_PHONE, MOBILE_PHONE, ADDRESS_ALIAS, STATE);
    }

    @Then("^I check order history and details$")
    public void thenICheckOrderHistoryAndDetails() {
        WebElement my_order = myAccountPage.checkOrderHistoryAndDetails();
        Assert.assertNotNull(my_order);
    }
}
