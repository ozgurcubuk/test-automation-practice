package com.automationpractice.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CategoryPage extends AbstractBasePage {

    Actions actions = createActionObject(driver);
    WebDriverWait webDriverWait = createWebDriverWaitObject(driver);

    public CategoryPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.XPATH, using = "//*[@id=\"center_column\"]/ul/li[1]")
    WebElement first_product_on_category;

    @FindBy(how = How.XPATH, using = "//*[@id=\"center_column\"]/ul/li[1]/div/div[2]/div[2]/a[1]")
    WebElement add_to_chart_button_of_first_product;

    @FindBy(how = How.CLASS_NAME, using = "continue")
    WebElement continue_shopping_button;

    @FindBy(how = How.ID, using = "search_query_top")
    WebElement search_text_field;

    @FindBy(how = How.NAME, using = "submit_search")
    WebElement search_button;

    @FindBy(how = How.XPATH, using = "//*[@title='View my shopping cart']")
    WebElement shopping_chart;

    @FindBy(how = How.ID, using = "button_order_cart")
    WebElement shopping_chart_check_out_button;

    public void addProductToChart() {
        actions.moveToElement(first_product_on_category).build().perform();
        add_to_chart_button_of_first_product.click();
        webDriverWait.until(ExpectedConditions.visibilityOf(continue_shopping_button)).click();
    }

    public void searchProductAndAddToChart(String searchQuery) {
        search_text_field.sendKeys(searchQuery);
        search_button.click();
        actions.moveToElement(first_product_on_category).build().perform();
        add_to_chart_button_of_first_product.click();
        webDriverWait.until(ExpectedConditions.visibilityOf(continue_shopping_button)).click();
    }

    public void checkOutChart() {
        actions.moveToElement(shopping_chart).build().perform();
        webDriverWait.until(ExpectedConditions.visibilityOf(shopping_chart_check_out_button)).click();
    }
}
