package com.automationpractice.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class MyAccountPage extends AbstractBasePage {

    Actions actions = createActionObject(driver);
    WebDriverWait webDriverWait = createWebDriverWaitObject(driver);

    public MyAccountPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.XPATH, using = "//*[@id=\"block_top_menu\"]/ul/li[2]/a") //link_text ve daha generic xpath denedim ancak locate etmedeki sorun devam ettiği ve zaman kaybına sebep olduğu için bu şekilde verdim
            WebElement dresses_category;

    @FindBy(how = How.XPATH, using = "//*[@id=\"block_top_menu\"]/ul/li[2]/ul/li[3]/a") //link_text ve daha generic xpath denedim ancak locate etmedeki sorun devam ettiği ve zaman kaybına sebep olduğu için bu şekilde verdim
            WebElement summer_dresses;

    @FindBy(how = How.CLASS_NAME, using = "header_user_info")
    WebElement user_account_avatar;

    @FindBy(how = How.XPATH, using = "//span[.='Order history and details']")
    WebElement order_history_and_details_button;

    @FindBy(how = How.CLASS_NAME, using = "first_item")
    WebElement first_item_on_my_order_history;

    public void clickToDressesCategory() {
        actions.moveToElement(dresses_category).build().perform();
        webDriverWait.until(ExpectedConditions.visibilityOf(summer_dresses));
        summer_dresses.click();
    }

    public WebElement checkOrderHistoryAndDetails() {
        user_account_avatar.click();
        order_history_and_details_button.click();
        return first_item_on_my_order_history;
    }
}
