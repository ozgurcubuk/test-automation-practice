package com.automationpractice.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractBasePage {

    WebDriver driver;
    Actions actions;
    WebDriverWait webDriverWait;

    public AbstractBasePage(WebDriver driver) {
        this.driver = driver;
    }

    public Actions createActionObject(WebDriver driver) {
        if (actions == null) {
            actions = new Actions(driver);
        }
        return actions;
    }

    public WebDriverWait createWebDriverWaitObject(WebDriver driver) {
        if (webDriverWait == null) {
            webDriverWait = new WebDriverWait(driver, 5);
        }
        return webDriverWait;
    }
}
