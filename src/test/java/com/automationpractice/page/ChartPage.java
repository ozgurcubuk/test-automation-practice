package com.automationpractice.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChartPage extends AbstractBasePage{

    public ChartPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.XPATH, using = "//*[@id=\"center_column\"]/p[2]/a[1]")  //Bu elementi birçok yöntemle bulmaya çalıştım ancak bu şekilde bulabildi.
            WebElement proceed_to_address_button;

    @FindBy(how = How.NAME, using = "processAddress")
    WebElement proceed_to_shipping_button;

    @FindBy(how = How.ID, using = "cgv")
    WebElement agree_terms_of_service_for_shipping_checkbox;

    @FindBy(how = How.NAME, using = "processCarrier")
    WebElement proceed_to_payment_button;

    @FindBy(how = How.CLASS_NAME, using = "bankwire")
    WebElement bank_wire_option;

    @FindBy(how = How.XPATH, using = "//span[.='I confirm my order']")
    WebElement confirm_order_button;

    public void continueToAddressSection() {
        proceed_to_address_button.click();
    }

    public void continueToShippingSectionAndAggreeTerms() {
        proceed_to_shipping_button.click();
        agree_terms_of_service_for_shipping_checkbox.click();
    }

    public void continueToPaymentSection() {
        proceed_to_payment_button.click();
        bank_wire_option.click();
        confirm_order_button.click();
    }
}
