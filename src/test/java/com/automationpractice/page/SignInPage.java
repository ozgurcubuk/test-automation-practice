package com.automationpractice.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class SignInPage extends AbstractBasePage {

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(how = How.ID, using = "email_create")
    WebElement email_text_field;

    @FindBy(how = How.ID, using = "SubmitCreate")
    WebElement create_an_account_button;

    @FindBy(how = How.ID, using = "id_gender1")
    WebElement mr_radio_button;

    @FindBy(how = How.ID, using = "id_gender2")
    WebElement mrs_radio_button;

    @FindBy(how = How.ID, using = "customer_firstname")
    WebElement first_name_text_field;

    @FindBy(how = How.ID, using = "customer_lastname")
    WebElement last_name_text_field;

    @FindBy(how = How.ID, using = "passwd")
    WebElement password_text_field;

    @FindBy(how = How.ID, using = "firstname")
    WebElement address_first_name_text_field;

    @FindBy(how = How.ID, using = "lastname")
    WebElement address_last_name_text_field;

    @FindBy(how = How.ID, using = "company")
    WebElement company_text_field;

    @FindBy(how = How.ID, using = "address1")
    WebElement address_line1_text_field;

    @FindBy(how = How.ID, using = "address2")
    WebElement address_line2_text_field;

    @FindBy(how = How.ID, using = "city")
    WebElement city_text_field;

    @FindBy(how = How.ID, using = "postcode")
    WebElement postal_code_text_field;

    @FindBy(how = How.ID, using = "phone")
    WebElement home_phone_text_field;

    @FindBy(how = How.ID, using = "phone_mobile")
    WebElement mobile_phone_text_field;

    @FindBy(how = How.ID, using = "alias")
    WebElement address_alias_text_field;

    @FindBy(how = How.ID, using = "id_state")
    WebElement state_drop_down;

    @FindBy(how = How.ID, using = "submitAccount")
    WebElement register_button;

    public void enterAnEmailAndClickToCreateAnAccountButton() {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        email_text_field.sendKeys(UUID.randomUUID().toString().replace("-", "") + "@gmail.com");
        create_an_account_button.click();
    }

    public void fillInUserInformations(String gender, String firstName, String lastName, String password,
                                       String company, String address1, String address2, String city,
                                       String postal_code, String home_phone, String mobile_phone,
                                       String address_alias, String state) {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        if (gender.equalsIgnoreCase("MR")) {
            mr_radio_button.click();
        } else if (gender.equalsIgnoreCase("MRS")) {
            mrs_radio_button.click();
        }
        first_name_text_field.sendKeys(firstName);
        last_name_text_field.sendKeys(lastName);
        password_text_field.sendKeys(password);
        company_text_field.sendKeys(company);
        address_line1_text_field.sendKeys(address1);
        address_line2_text_field.sendKeys(address2);
        city_text_field.sendKeys(city);
        postal_code_text_field.sendKeys(postal_code);
        home_phone_text_field.sendKeys(home_phone);
        mobile_phone_text_field.sendKeys(mobile_phone);
        address_alias_text_field.clear();
        address_alias_text_field.sendKeys(address_alias);
        state_drop_down.sendKeys(state);
        register_button.click();
    }
}
